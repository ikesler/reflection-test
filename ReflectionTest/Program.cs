﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace ReflectionTest
{
    /// <summary>
    /// Compare different implementation of serialization.
    /// </summary>
    class Program
    {
        private const int NumberOfIteration = 10000;
        static void Main(string[] args)
        {
            TimeSpan delta;

            delta = Iterate(() =>
            {
                var obj = GetDataObject();
                string value = obj.ToString();
            });
            Console.WriteLine("ToString: " + delta);

            delta = Iterate(() =>
            {
                var obj = GetDataObject();
                string value = Serialize(obj);
            });
            Console.WriteLine("Custom Serialize: " + delta);

            delta = Iterate(() =>
            {
                var obj = GetDataObject();
                string value = ToJson(obj);
            });
            Console.WriteLine("JSON.NET: " + delta);


            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString))
            {
                connection.Open();

                delta = Iterate(() =>
                {
                    var command = new SqlCommand
                    {
                        CommandText = "SELECT COUNT(*) FROM TestTable",
                        Connection = connection
                    };
                    command.ExecuteScalar();
                });
                Console.WriteLine("DB request: " + delta);
            }

            using (var context = new ReflectionTestEntities())
            {
                delta = Iterate(() =>
                {
                    var count = context.TestTables.Count();
                });
                Console.WriteLine("EF DB request: " + delta);
            }

            Console.ReadLine();
        }

        static TimeSpan Iterate(Action action)
        {
            var start = DateTime.Now;
            for (var i = 0; i < NumberOfIteration; ++i)
            {
                action();
            }
            return DateTime.Now - start;
        }

        static DataObject GetDataObject()
        {
            var result = new DataObject
            {
                String = new string('a', 500),
                InnerObject = new DataObject
                {
                    InnerObject = new DataObject()
                },
                InnerList = new List<DataObject>()
            };
            for (var i = 0; i < 10; ++i)
            {
                result.InnerList.Add(new DataObject());
            }
            return result;
        }

        static string Serialize(object obj, StringBuilder result = null)
        {
            bool shouldReturn = result == null;
            result = result ?? new StringBuilder();
            if (obj == null)
            {
                result.Append("null");
            }
            else
            {
                var type = obj.GetType();
                if (type.FullName.StartsWith("System."))
                {
                    if (obj is ICollection)
                    {
                        result.Append("[");
                        foreach (var item in obj as IEnumerable)
                        {
                            Serialize(item, result);
                            result.Append(",");
                        }
                        result.Append("]");
                    }
                    else
                    {
                        result.Append(obj);
                    }
                }
                else
                {
                    var properties = type.GetProperties();
                    foreach (var property in properties)
                    {
                        var value = property.GetValue(obj);
                        result.AppendLine(string.Format("{0} => {1}:", type, property.Name));
                        Serialize(value, result);
                    }
                }
            }
            return shouldReturn ?  result.ToString() : null;
        }

        public static string ToJson(object obj)
        {
            JsonSerializer serializer = new JsonSerializer();
            StringBuilder builder = new StringBuilder();
            TextWriter writer = new StringWriter(builder);
            serializer.Serialize(writer, obj);
            return builder.ToString();
        }
    }

    class DataObject
    {
        public string String { get; set; }
        public int Int { get; set; }
        public DateTime DateTime { get; set; }
        public DataObject InnerObject { get; set; }
        public List<DataObject> InnerList { get; set; }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(string.Format("DataObject => String: {0}", String));
            builder.AppendLine(string.Format("DataObject => Int: {0}", Int));
            builder.AppendLine(string.Format("DataObject => DateTime: {0}", DateTime));
            builder.AppendLine(string.Format("DataObject => InnerObject: {0}", InnerObject));
            builder.AppendLine("DataObject => InnerList:");

            if (InnerList != null)
            {
                foreach (var obj in InnerList)
                {
                    builder.Append(obj);
                    builder.Append(";");
                }
            }
            else
            {
                builder.Append("null");
            }

            return builder.ToString();
        }
    }
}
